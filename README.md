#ApiRestful-Server

Rutas Rest expuestas en: http//:localhost:3000/

Ruta: "/places"

.get  -> Obtener todos los registros paginados

.post -> Dar de alta un registro

Ruta ""/:id"

.get    -> Obtener un registro específico

.put    -> Actualizar un registro específico

.delete -> Eliminar un registro específico

Formato de información Json


Reconstruir modulos de node
```
npm install

```

Levantar base de datos mongodb

```
sudo service mongod start
mongod

```
Levantar el Servidor, cualquiera de estos dos comandos

```
npm run start

```

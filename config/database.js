const mongoose = require('mongoose');
const dbName = 'dbdesarrollo';

module.exports = {
  connect: ()=> mongoose.connect('mongodb://localhost/' + dbName,{ useNewUrlParser: true, useUnifiedTopology: true }),
  dbName,
  connection: ()=> {
    if (mongose.connection)
        return mongoose.connection;
    return this.connect();
  }
}

//Importar modelo
const Place = require('../Models/Place');

function find(req,res,next) {
  Place.findById(req.params.id)
    .then(place=>{
      req.place = place;
      next();
    }).catch(err=>{
      next(err);
    });
}

function  index(req,res){
  //Todos los lugares
  Place.paginate({},{ page: req.query.page || 1, limit:8, sort: {'_id': -1} }).then(docs=>{
     res.json(docs);
   }).catch(err=>{
     console.log(err);
     res.json(err);
   });
}

function create(req,res) {
  //Crear nuevos lugares
  Place.create({
    title: req.body.title,
    description: req.body.description,
    acceptsCreditCard: req.body.acceptsCreditCard,
    openHour: req.body.openHour,
    closeHour: req.body.closeHour
    }).then(doc=>{
      res.json(doc)
    }).catch(err=>{
      console.log(err);
      res.json(err);
    });
}

function show(req,res) {
  //Busqueda individual
  // Place.findById(req.params.id).then(doc=>{
  //       res.json(doc);
  //     })
  //     .catch(err=>{
  //       res.json(err);
  //     });
  res.json(req.place);
}

function update(req,res) {
  //Actualizar un recurso
  let attributes = ['title','description','acceptsCreditCard','openHour','closeHour'];
  let placeParams = {};
  attributes.forEach(attr=>{
    if(Object.prototype.hasOwnProperty.call(req.body,attr))
      placeParams[attr] = req.body[attr];
  });
req.place = Object.assign(req.place, placeParams);

 //Place.findByIdAndUpdate(req.params.id,placeParams,{new: true}).then(doc=>{
   req.place.save().then(doc=>{
   res.json(doc);
   }).catch(err=>{
     console.log(err);
     res.json(err);
   });
}

function destroy(req,res) {
  //Eliminar recursos
  // let docresp={"id":req.params.id,"mensaje": "Eliminado"};
  //
  // Place.findByIdAndRemove(req.params.id).then(doc=>{
  //resp.json(docresp);
  req.place.remove()
      .then(doc=>{
        res.json({})
    }).catch(err=>{
      console.log(err);
      resp.json(err);
    });
}

//exports
module.exports = {index,create,show,update,destroy,find};

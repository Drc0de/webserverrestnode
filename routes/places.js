const express = require('express');

/*config route*/
/*I.Routes*/
let router = express.Router();
//importamos la logica de controllador
const placesController = require('../controllers/PlacesController');


router.route('/')
  .get(placesController.index)
  .post(placesController.create)
router.route('/:id')
.get(placesController.find, placesController.show)
.put(placesController.find, placesController.update)
.delete(placesController.find, placesController.destroy)
/*F.Routes*/

module.exports = router;

var request = require('supertest')
,app = require('../app')

describe("Places", function() {
  it("Get Places with json", function (done){
     request(app)
     .get("/places")
     .set('Accept', 'application/json')
     .expect('Content-Type', /json/)
     .expect(200,done)
     console.log(done);
  });
});
